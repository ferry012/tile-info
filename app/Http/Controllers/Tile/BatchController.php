<?php

namespace App\Http\Controllers\Tile;

use App\BatchNumber;
use App\Http\Controllers\Controller;
use App\Http\Resources\BatchCollection;
use Illuminate\Http\Request;

class BatchController extends Controller
{
    public function allBatch(Request $request)
    {
        if($request->showAll) {
            $query = BatchNumber::orderBy('created_at','desc')->get();
        }else {
            $searchValue = $request->search;
            $orderBy = $request->sortby;
            $orderByDir = $request->sortdir;
            $perPage = $request->currentpage;
            $filter = $request->filterSearch;

            if($filter === 'batchNumber'){
                $query = BatchNumber::where(function($q) use($searchValue){
                    $q->orwhere('number', 'LIKE', "%$searchValue%");
                })
                    ->orderBy($orderBy, $orderByDir)->paginate($perPage);
            }
           else if($filter === 'EAN11'){
                $query = BatchNumber::where(function($q) use($searchValue){
                    $q->orwhere('EAN11', 'LIKE', "%$searchValue%");
                })
                    ->orderBy($orderBy, $orderByDir)->paginate($perPage);
            }
           else if($filter === 'MATNR'){
               $query = BatchNumber::where(function($q) use($searchValue){
                   $q->orwhere('EAN11', 'LIKE', "%$searchValue%");
               })
                   ->orderBy($orderBy, $orderByDir)->paginate($perPage);
           }
           else if($filter === 'MAKTX'){
               $query = BatchNumber::where(function($q) use($searchValue){
                   $q->orwhere('MAKTX', 'LIKE', "%$searchValue%");
               })
                   ->orderBy($orderBy, $orderByDir)->paginate($perPage);
           }
           else if($filter === 'code'){
               $query = BatchNumber::where(function($q) use($searchValue){
                   $q->orwhere('code', 'LIKE', "%$searchValue%");
               })
                   ->orderBy($orderBy, $orderByDir)->paginate($perPage);
           }
           else{
               $query = BatchNumber::orderBy($orderBy, $orderByDir)->paginate($perPage);
           }

        }
        return new BatchCollection($query);
    }
}
