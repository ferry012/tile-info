<?php

namespace App\Http\Controllers\Tile;

use App\Brand;
use App\Http\Controllers\Controller;
use App\Http\Resources\BrandCollection;
use App\Http\Resources\TradeMarkCollection;
use App\TradeMark;
use Illuminate\Http\Request;
use Illuminate\Queue\RedisQueue;
use Image;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return BrandCollection
     */
    public function index(Request $request)
    {

        if($request->showAll) {
            $query = TradeMark::orderBy('trademark.brand_name','asc')->get();
        }else {

            $searchValue = $request->search;
            $orderBy = $request->sortby;
            $orderByDir = $request->sortdir;
            $perPage = $request->currentpage;

            $query = TradeMark::where('trademark.brand_name', 'LIKE', "%$searchValue%")
                ->orderBy($orderBy, $orderByDir)
                ->paginate($perPage);

        }
        return new TradeMarkCollection($query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {

//        $validateData = $request->validate([
//            'photo' => 'image|mimes:jpeg,jpg',
//            'brand' => 'required',
//        ]);
        if ($request->photo) {
            $position = strpos($request->photo, ';');
            $sub = substr($request->photo, 0, $position);
            $ext = explode('/', $sub)[1];

            $name = time().".".$ext;
            $img = Image::make($request->photo)->resize(288,192); // Width and Height
            $upload_path = 'backend/brand/';
            $image_url = $upload_path.$name;
            $img->save($image_url);

//            dd($request->brand,$image_url);
//            $trademark = new TradeMark;
//            $trademark->brand_name = $request->brand;
//            $trademark->photo = $image_url;
//            $trademark->save();

//
            $data = array();
            $data['brand_name'] = $request->brand;
            $data['photo'] = $image_url;

            $query = TradeMark::create($data);
        }

        return response()->json([
            'status' => 'success',
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $query = TradeMark::where('id',$id)->first();
        return response()->json($query);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
    }

    public function updateBrand(Request $request)
    {
        $data = array();
        $id = $request->id;
        $data['brand_name'] = $request->brand_name;
        $image = $request->photo;
        if ($image) {
            $position = strpos($image, ';');
            $sub = substr($image, 0, $position);
            $ext = explode('/', $sub)[1];

            $name = time().".".$ext;
            $img = Image::make($image)->resize(240,200);
            $upload_path = 'backend/brand/';
            $image_url = $upload_path.$name;
            $success = $img->save($image_url);

            if ($success) {
                $data['photo'] = $image_url;
                $img = TradeMark::where('id',$id)->first();
                $image_path = $img->photo;
//                $done = unlink($image_path);
                $user  = TradeMark::where('id',$id)->update($data);
            }

        }else{
            $oldphoto = $request->photo;
            $data['photo'] = $oldphoto;
            $user  = TradeMark::where('id',$id)->update($data);
        }
        $query = TradeMark::where('id',$id)->update($data);
        return response()->json([
            'status' => 'success',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = TradeMark::where('id',$id)->delete();
    }
    public function clickImage(Request $request){
        $click = TradeMark::where('id',$request->id)->first();
        return response()->json($click);
    }
}
