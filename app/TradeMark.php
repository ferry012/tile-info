<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TradeMark extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'trademark';
    protected $fillable = ['brand_name','photo'];
}
