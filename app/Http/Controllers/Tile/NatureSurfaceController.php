<?php

namespace App\Http\Controllers\Tile;

use App\Http\Controllers\Controller;
use App\Http\Resources\NatureSurfaceCollection;
use App\NatureSurface;
use Illuminate\Http\Request;

class NatureSurfaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->showAll) {
            $query = NatureSurface::orderBy('surface','desc')->get();
        }else {
            $searchValue = $request->search;
            $orderBy = $request->sortby;
            $orderByDir = $request->sortdir;
            $perPage = $request->currentpage;
            $query = NatureSurface::where('surface', 'LIKE', "%$searchValue%")
                ->orderBy($orderBy, $orderByDir)->paginate($perPage);
        }
        return new NatureSurfaceCollection($query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = array();
        $data['surface'] = $request->surface;
        $query = NatureSurface::create($data);
        return response()->json([
            'status' => 'success',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query = NatureSurface::where('id',$id)->first();
        return response()->json($query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = NatureSurface::where('id',$id)->delete();
    }
    public function updateNature(Request $request){
        $data = array();
        $id = $request->id;
        $data['surface'] = $request->surface;
        $query = NatureSurface::where('id',$id)->update($data);
        return response()->json([
            'status' => 'success',
        ]);
    }
}
