<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BatchNumber extends Model
{
    protected $table ='batch_number';
    protected $fillable = ['number','EAN11','MATNR','MAKTX','code'];
}
