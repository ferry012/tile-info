<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTileMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tile_master', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('EAN11');
            $table->string('MATNR');
            $table->string('MAKTX');
            $table->string('brand');
            $table->string('code');
            $table->string('importers_name');
            $table->string('importers_address');
            $table->string('manu_name');
            $table->string('manu_address');
            $table->string('country_origin');
            $table->string('nominal_size');
            $table->string('thickness');
            $table->string('water_range');
            $table->string('method_shaping');
            $table->string('kalikasan');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tile_master');
    }
}
