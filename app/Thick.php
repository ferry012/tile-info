<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thick extends Model
{
    protected $table = 'thickness';
    protected $fillable = ['density'];
}
