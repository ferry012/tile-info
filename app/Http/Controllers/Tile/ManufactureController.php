<?php

namespace App\Http\Controllers\Tile;

use App\Http\Controllers\Controller;
use App\Http\Resources\ManufactureCollection;
use App\Manufacture;
use Illuminate\Http\Request;

class ManufactureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->showAll) {
            $query = Manufacture::orderBy('name','desc')->get();
        }else {
            $searchValue = $request->search;
            $orderBy = $request->sortby;
            $orderByDir = $request->sortdir;
            $perPage = $request->currentpage;
            $query = Manufacture::where('name', 'LIKE', "%$searchValue%")
                ->orWhere('address', 'LIKE', "%$searchValue%")
                ->orderBy($orderBy, $orderByDir)->paginate($perPage);
        }
        return new ManufactureCollection($query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = array();
        $data['name'] = $request->name;
        $data['address'] = $request->address;
        $query = Manufacture::create($data);
        return response()->json([
            'status' => 'success',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query = Manufacture::where('id',$id)->first();
        return response()->json($query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = Manufacture::where('id',$id)->delete();
    }
    public function updateManu(Request $request)
    {
        $data = array();
        $id = $request->id;
        $data['name'] = $request->name;
        $data['address'] = $request->address;
        $query = Manufacture::where('id',$id)->update($data);
        return response()->json([
            'status' => 'success',
        ]);
    }
}
