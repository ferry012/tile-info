<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hold extends Model
{
    protected $table = 'hold';

    protected $fillable = [
        'pro_id',
        'pro_barcode',
        'pro_name',
        'pro_quantity',
        'product_price',
        'sub_total',
        'discount'
    ];
}
