<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Marketing extends Model
{
    protected $table = 'marketing';
    protected $fillable = [
        'plantbranch_id','photo'
    ];
}
