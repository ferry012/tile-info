<?php

namespace App\Http\Controllers\Tile;

use App\Brand;
use App\Country;
use App\Http\Controllers\Controller;
use App\Importers;
use App\Manufacture;
use App\MethodShaping;
use App\NatureSurface;
use App\Nominal;
use App\Thick;
use App\Tiles;
use App\WaterAbsorption;
use Illuminate\Http\Request;

class GetAllDataController extends Controller
{
    public function getBrand(){
        $query = Brand::all();
        return response()->json($query);
    }
    public function getImporter(){
        $query = Importers::all();
        return response()->json($query);
    }
    public function getManu(){
        $query = Manufacture::all();
        return response()->json($query);
    }
    public function getCountry(){
        $query = Country::all();
        return response()->json($query);
    }
    public function getNominal(){
        $query = Nominal::all();
        return response()->json($query);
    }
    public function getThick(){
        $query = Thick::all();
        return response()->json($query);
    }
    public function getWaterRange(){
        $query = WaterAbsorption::all();
        return response()->json($query);
    }
    public function getMethod(){
        $query = MethodShaping::all();
        return response()->json($query);
    }
    public function getNature(){
        $query = NatureSurface::all();
        return response()->json($query);
    }
    //Tiles Per Pack
    public function getTotalPack(){
        $query = Tiles::all();
        return response()->json($query);
    }
}
