<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NatureSurface extends Model
{
    protected $table = 'nature_surface';
    protected $fillable = ['surface'];
}
