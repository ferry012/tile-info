<?php

namespace App\Http\Controllers\Tile;

use App\Http\Controllers\Controller;
use App\Http\Resources\ImportersCollection;
use App\Importers;
use Illuminate\Http\Request;

class ImportersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->showAll) {
            $query = Importers::orderBy('name','desc')->get();
        }else {
            $searchValue = $request->search;
            $orderBy = $request->sortby;
            $orderByDir = $request->sortdir;
            $perPage = $request->currentpage;
            $query = Importers::where('name', 'LIKE', "%$searchValue%")
                ->orWhere('address', 'LIKE', "%$searchValue%")
                ->orderBy($orderBy, $orderByDir)->paginate($perPage);
        }
        return new ImportersCollection($query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $data = array();
        $data['name'] = $request->name;
        $data['address'] = $request->address;
        $query = Importers::create($data);
        return response()->json([
            'status' => 'success',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query = Importers::where('id',$id)->first();
        return response()->json($query);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, cr $cr)
    {
        //
    }

    public function destroy($id)
    {
        $query = Importers::where('id',$id)->delete();
    }
    public function updateImporters(Request $request)
    {
        $data = array();
        $id = $request->id;
        $data['name'] = $request->name;
        $data['address'] = $request->address;
        $query = Importers::where('id',$id)->update($data);
        return response()->json([
            'status' => 'success',
        ]);
    }
}
