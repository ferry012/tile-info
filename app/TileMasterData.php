<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TileMasterData extends Model
{
    protected $table='MATERIAL_MASTER';
    protected $connection ='sqlsrv2';
}
