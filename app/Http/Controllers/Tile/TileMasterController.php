<?php

namespace App\Http\Controllers\Tile;

use App\Brand;
use App\Country;
use App\Http\Controllers\Controller;
use App\Http\Resources\TilesMasterCollection;
use App\Importers;
use App\Manufacture;
use App\MethodShaping;
use App\NatureSurface;
use App\Nominal;
use App\Thick;
use App\TileMaster;
use App\Tiles;
use App\WaterAbsorption;
use Illuminate\Http\Request;

class TileMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //Tile Information
    public function index(Request $request)
    {
        if ($request->showAll) {
            $query = TileMaster::orderBy('created_at', 'desc')->get();
        } else {

            $searchValue = $request->search;
            $orderBy = $request->sortby;
            $orderByDir = $request->sortdir;
            $perPage = $request->currentpage;
            $filter = $request->filterSearch;

            $searchEAN11 = $request->searchEAN11;
            $searchMATNR = $request->searchMATNR;
            $searchMAKTX = $request->searchMAKTX;
            $searchBrand = $request->searchBrand;
            $searchImporters_name = $request->searchImporters_name;
            $searchManu_name = $request->searchManu_name;
            $searchCountry_origin = $request->searchCountry_origin;
            $searchNominal_size = $request->searchNominal_size;
            $searchThickness = $request->searchThickness;
            $searchWater_range = $request->searchWater_range;
            $searchMethod_shaping = $request->searchMethod_shaping;
            $searchNature_surface = $request->searchNature_surface;
            $searchTiles_per_pack = $request->searchTiles_per_pack;
            $searchCode = $request->searchCode;

       if($searchValue !== null){
           $query = TileMaster::where(function ($q) use ($searchValue) {
               $q->orwhere('EAN11', 'LIKE', "%$searchValue%")
                   ->orwhere('MATNR', 'LIKE', "%$searchValue%")
                   ->orwhere('MAKTX', 'LIKE', "%$searchValue%")
                   ->orwhere('code', 'LIKE', "%$searchValue%")
                   ->orwhere('brand', 'LIKE', "%$searchValue%")
                   ->orwhere('importers_name', 'LIKE', "%$searchValue%")
                   ->orwhere('manu_name', 'LIKE', "%$searchValue%")
                   ->orwhere('country_origin', 'LIKE', "%$searchValue%")
                   ->orwhere('nominal_size', 'LIKE', "%$searchValue%")
                   ->orwhere('thickness', 'LIKE', "%$searchValue%")
                   ->orwhere('water_range', 'LIKE', "%$searchValue%")
                   ->orwhere('method_shaping', 'LIKE', "%$searchValue%")
                   ->orwhere('kalikasan', 'LIKE', "%$searchValue%")
                   ->orwhere('tiles_per_pack', 'LIKE', "%$searchValue%");
           })
               ->orderBy($orderBy, $orderByDir)->paginate($perPage);
       }

       else if($searchEAN11 !== null || $searchMATNR !== null || $searchMAKTX !== null || $searchBrand !== null
            || $searchImporters_name !== null || $searchManu_name !== null || $searchCountry_origin !== null
            || $searchNominal_size !== null ||$searchThickness !== null || $searchWater_range !== null || $searchMethod_shaping !== null
            || $searchNature_surface !== null || $searchTiles_per_pack !== null || $searchCode !== null){

            if ($searchEAN11 !== null) {
                $query = TileMaster::where(function ($q) use ($searchEAN11) {
                    $q->orwhere('EAN11', 'LIKE', "%$searchEAN11%");
                })
                    ->orderBy($orderBy, $orderByDir)->paginate($perPage);
            }
            if ($searchMATNR !== null) {
                $query = TileMaster::where(function ($q) use ($searchMATNR) {
                    $q->orwhere('MATNR', 'LIKE', "%$searchMATNR%");
                })
                    ->orderBy($orderBy, $orderByDir)->paginate($perPage);
            }
            if ($searchMAKTX !== null) {
                $query = TileMaster::where(function ($q) use ($searchMAKTX) {
                    $q->orwhere('MAKTX', 'LIKE', "%$searchMAKTX%");
                })
                    ->orderBy($orderBy, $orderByDir)->paginate($perPage);
            }
            if ($searchBrand !== null) {
                $query = TileMaster::where(function ($q) use ($searchBrand) {
                    $q->orwhere('brand', 'LIKE', "%$searchBrand%");
                })
                    ->orderBy($orderBy, $orderByDir)->paginate($perPage);
            }
            if ($searchImporters_name !== null) {
                $query = TileMaster::where(function ($q) use ($searchImporters_name) {
                    $q->orwhere('importers_name', 'LIKE', "%$searchImporters_name%");
                })
                    ->orderBy($orderBy, $orderByDir)->paginate($perPage);
            }
            if ($searchManu_name !== null) {
                $query = TileMaster::where(function ($q) use ($searchManu_name) {
                    $q->orwhere('manu_name', 'LIKE', "%$searchManu_name%");
                })
                    ->orderBy($orderBy, $orderByDir)->paginate($perPage);
            }
            if ($searchCountry_origin !== null) {
                $query = TileMaster::where(function ($q) use ($searchCountry_origin) {
                    $q->orwhere('country_origin', 'LIKE', "%$searchCountry_origin%");
                })
                    ->orderBy($orderBy, $orderByDir)->paginate($perPage);
            }
            if ($searchNominal_size !== null) {
                $query = TileMaster::where(function ($q) use ($searchNominal_size) {
                    $q->orwhere('nominal_size', 'LIKE', "%$searchNominal_size%");
                })
                    ->orderBy($orderBy, $orderByDir)->paginate($perPage);
            }
            if ($searchThickness !== null) {
                $query = TileMaster::where(function ($q) use ($searchThickness) {
                    $q->orwhere('thickness', 'LIKE', "%$searchThickness%");
                })
                    ->orderBy($orderBy, $orderByDir)->paginate($perPage);
            }
            if ($searchWater_range !== null) {
                $query = TileMaster::where(function ($q) use ($searchWater_range) {
                    $q->orwhere('water_range', 'LIKE', "%$searchWater_range%");
                })
                    ->orderBy($orderBy, $orderByDir)->paginate($perPage);
            }
            if ($searchMethod_shaping !== null) {
                $query = TileMaster::where(function ($q) use ($searchMethod_shaping) {
                    $q->orwhere('method_shaping', 'LIKE', "%$searchMethod_shaping%");
                })
                    ->orderBy($orderBy, $orderByDir)->paginate($perPage);
            }
            if ($searchNature_surface !== null) {
                $query = TileMaster::where(function ($q) use ($searchNature_surface) {
                    $q->orwhere('kalikasan', 'LIKE', "%$searchNature_surface%");
                })
                    ->orderBy($orderBy, $orderByDir)->paginate($perPage);
            }
            if ($searchTiles_per_pack !== null) {
                $query = TileMaster::where(function ($q) use ($searchTiles_per_pack) {
                    $q->orwhere('tiles_per_pack', 'LIKE', "%$searchTiles_per_pack%");
                })
                    ->orderBy($orderBy, $orderByDir)->paginate($perPage);
            }
            if ($searchCode !== null) {
                $query = TileMaster::where(function ($q) use ($searchCode) {
                    $q->orwhere('code', 'LIKE', "%$searchCode%");
                })
                    ->orderBy($orderBy, $orderByDir)->paginate($perPage);
            }
        }
            else {

                $query = TileMaster::orderBy($orderBy, $orderByDir)->paginate($perPage);
            }

        }
        return new TilesMasterCollection($query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $importers = Importers::where('id', '=', $request->importers_id)
            ->select('name as import_name', 'address as import_address')
            ->first();

        $manu = Manufacture::where('id', '=', $request->manu_id)
            ->select('name as manu_name', 'address as manu_address')
            ->first();

        $brand = Brand::where('id', '=', $request->brand)
            ->select('brand_name')
            ->first();

        $country = Country::where('id', '=', $request->country_origin)
            ->select('country_name')
            ->first();

        $nominal = Nominal::where('id', '=', $request->nominal_size)
            ->select('size')
            ->first();

        $thickness = Thick::where('id', '=', $request->thickness)
            ->select('density')
            ->first();

        $water = WaterAbsorption::where('id', '=', $request->water_range)
            ->select('range')
            ->first();

        $method = MethodShaping::where('id', '=', $request->method_shaping)
            ->select('shaping')
            ->first();

        $kalikasan = NatureSurface::where('id', '=', $request->method_shaping)
            ->select('surface')
            ->first();

        $total_per = Tiles::where('id', '=',$request->tiles_per_pack)
            ->select('per_pack')
            ->first();

        $data = array();
        $data['EAN11'] = $request->EAN11;
        $data['MATNR'] = $request->MATNR;
        $data['MAKTX'] = $request->MAKTX;
        $data['brand'] = $brand->brand_name;
        $data['importers_name'] = $importers->import_name;
        $data['importers_address'] = $importers->import_address;
        $data['manu_name'] = $manu->manu_name;
        $data['manu_address'] = $manu->manu_address;
        $data['country_origin'] = $country->country_name;
        $data['nominal_size'] = $nominal->size;
        $data['thickness'] = $thickness->density;
        $data['water_range'] = $water->range;
        $data['method_shaping'] = $method->shaping;
        $data['kalikasan'] = $kalikasan->surface;
        $data['tiles_per_pack'] = $total_per->per_pack;
        $data['code'] = $request->code;
        $query = TileMaster::create($data);
        return response()->json([
            'status' => 'success',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query = TileMaster::where('id', $id)->first();
        return response()->json($query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = TileMaster::where('id', $id)->delete();
    }

    public function updateTileWithData(Request $request)
    {

        $checkImport = TileMaster::where('id', '=', $request->id)
            ->select('importers_name')
            ->first();

        $checkManu = TileMaster::where('id', '=', $request->id)
            ->select('manu_name')
            ->first();

        $checkBrand = TileMaster::where('id', '=', $request->id)
            ->select('brand')
            ->first();

        $checkCountry = TileMaster::where('id', '=', $request->id)
            ->select('country_origin')
            ->first();

        $checkNominal = TileMaster::where('id', '=', $request->id)
            ->select('nominal_size')
            ->first();

        $checkThickness = TileMaster::where('id', '=', $request->id)
            ->select('thickness')
            ->first();

        $checkWater = TileMaster::where('id', '=', $request->id)
            ->select('water_range')
            ->first();

        $checkMethod = TileMaster::where('id', '=', $request->id)
            ->select('method_shaping')
            ->first();

        $checkNature = TileMaster::where('id', '=', $request->id)
            ->select('kalikasan')
            ->first();

        $checkTotalPerPack = TileMaster::where('id', '=', $request->id)
            ->select('tiles_per_pack')
            ->first();


        $tile_master = TileMaster::find($request->id);
        $tile_master->EAN11 = $request->EAN11;
        $tile_master->MATNR = $request->MATNR;
        $tile_master->MAKTX = $request->MAKTX;

        if($checkBrand->brand === $request->brand){
            $brand = Brand::where('brand_name', '=', $request->brand)
                ->select('brand_name')
                ->first();
            $tile_master->brand = $brand->brand_name;
        }

        if($checkBrand->brand !== $request->brand){
            $brand = Brand::where('brand_name', '=', $request->brand)
                ->select('brand_name')
                ->first();
            $tile_master->brand = $brand->brand_name;
        }

        if ($checkImport->importers_name === $request->importers_name) {
            $importers = Importers::where('name', '=', $request->importers_name)
                ->select('name as import_name', 'address as import_address')
                ->first();
            $tile_master->importers_name = $importers->import_name;
            $tile_master->importers_address = $importers->import_address;
        }

        if ($checkImport->importers_name !== $request->importers_name) {

            $importers = Importers::where('name', '=', $request->importers_name)
                ->select('name as import_name', 'address as import_address')
                ->first();

            $tile_master->importers_name = $importers->import_name;
            $tile_master->importers_address = $importers->import_address;
        }
        if ($checkManu->manu_name === $request->manu_name) {

            $manu = Manufacture::where('name', '=', $request->manu_name)
                ->select('name as manu_name', 'address as manu_address')
                ->first();
            $tile_master->manu_name = $manu->manu_name;
            $tile_master->manu_address = $manu->manu_address;
        }
        if ($checkManu->manu_name !== $request->manu_name) {

            $manu = Manufacture::where('name', '=', $request->manu_name)
                ->select('name as manu_name', 'address as manu_address')
                ->first();
            $tile_master->manu_name = $manu->manu_name;
            $tile_master->manu_address = $manu->manu_address;
        }
        if($checkCountry->country_origin === $request->country_origin){
            $country = Country::where('country_name', '=', $request->country_origin)
                ->select('country_name')
                ->first();
            $tile_master->country_origin = $country->country_name;
        }

        if($checkCountry->country_origin !== $request->country_origin){
            $country = Country::where('country_name', '=', $request->country_origin)
                ->select('country_name')
                ->first();
            $tile_master->country_origin = $country->country_name;
        }

        if($checkNominal->nominal_size === $request->nominal_size){
            $nominal = Nominal::where('size', '=', $request->nominal_size)
                ->select('size')
                ->first();
            $tile_master->nominal_size = $nominal->size;
        }

        if($checkNominal->nominal_size !== $request->nominal_size){
            $nominal = Nominal::where('size', '=', $request->nominal_size)
                ->select('size')
                ->first();
            $tile_master->nominal_size = $nominal->size;
        }

        if($checkThickness->thickness === $request->thickness){
            $thick = Thick::where('density', '=', $request->thickness)
                ->select('density')
                ->first();
            $tile_master->thickness = $thick->density;
        }
        if($checkThickness->thickness !== $request->thickness){
            $thick = Thick::where('density', '=', $request->thickness)
                ->select('density')
                ->first();
            $tile_master->thickness = $thick->density;
        }

        if($checkWater->water_range === $request->water_range){
            $water = WaterAbsorption::where('range', '=', $request->water_range)
                ->select('range')
                ->first();
            $tile_master->water_range = $water->range;
        }
        if($checkWater->water_range !== $request->water_range){
            $water = WaterAbsorption::where('range', '=', $request->water_range)
                ->select('range')
                ->first();
            $tile_master->water_range = $water->range;
        }

        if($checkMethod->method_shaping === $request->method_shaping){
            $method = MethodShaping::where('shaping', '=', $request->method_shaping)
                ->select('shaping')
                ->first();
            $tile_master->method_shaping = $method->shaping;
        }

        if($checkMethod->method_shaping !== $request->method_shaping){
            $method = MethodShaping::where('shaping', '=', $request->method_shaping)
                ->select('shaping')
                ->first();
            $tile_master->method_shaping = $method->shaping;
        }

       if($checkNature->kalikasan === $request->kalikasan){
           $nature = NatureSurface::where('surface', '=', $request->kalikasan)
               ->select('surface')
               ->first();
           $tile_master->kalikasan = $nature->surface;
       }

        if($checkNature->kalikasan !== $request->kalikasan){
            $nature = NatureSurface::where('surface', '=', $request->kalikasan)
                ->select('surface')
                ->first();
            $tile_master->kalikasan = $nature->surface;
        }

        if($checkTotalPerPack->tiles_per_pack === $request->tiles_per_pack){
            $Total = Tiles::where('per_pack', '=', $request->tiles_per_pack)
                ->select('per_pack')
                ->first();

            $tile_master->tiles_per_pack = $Total->per_pack;
        }

        if($checkTotalPerPack->tiles_per_pack !== $request->tiles_per_pack){
            $Total = Tiles::where('per_pack', '=', $request->tiles_per_pack)
                ->select('per_pack')
                ->first();

            $tile_master->tiles_per_pack = $Total->per_pack;
        }

        $tile_master->code = $request->code;
        $tile_master->save();

        return response()->json([
            'status' => 'success',
        ]);
    }
}
