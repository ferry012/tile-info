<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('login', 'Api\Auth\AuthController@login')->name('login');
Route::post('oauth/token', 'Api\AccessTokenController@issueToken');
Route::post('refresh', 'Api\Auth\AuthController@refresh')->name('api.refresh');

Route::middleware('auth:api')->group(function () {

    Route::post('logout', 'Api\Auth\AuthController@logout')->name('logout');
    Route::post('register', 'Api\Auth\AuthController@register')->name('register');
    Route::get('auth-user', 'Api\Auth\AuthController@authenticatedUser')->name('auth-user');
    Route::patch('auth-user/update', 'Api\Auth\AuthController@authenticatedUserUpdate')->name('auth-user');
    Route::resource('users', 'Api\UserController');
    Route::resource('permissions', 'Api\PermissionController');
    Route::resource('roles', 'Api\RoleController');

});
//Brand
Route::resource('brand','Tile\BrandController');
Route::patch('/brand-update','Tile\BrandController@updateBrand');
Route::get('/click-image','Tile\BrandController@clickImage');

//Importers
Route::resource('importers','Tile\ImportersController');
Route::patch('/importers-update','Tile\ImportersController@updateImporters');

//Manufactures
Route::resource('manu','Tile\ManufactureController');
Route::patch('/manu-update','Tile\ManufactureController@updateManu');

//Country Of Origin
Route::resource('country','Tile\CountryController');
Route::patch('/country-update','Tile\CountryController@updateCountry');

//Nominal
Route::resource('nominal','Tile\NominalController');
Route::patch('/nominal-update','Tile\NominalController@updateNominal');

//Thickness
Route::resource('thick','Tile\ThickController');
Route::patch('/thick-update','Tile\ThickController@updateThick');

//Water Absorption
Route::resource('water','Tile\WaterAbsorptionController');
Route::patch('/water-update','Tile\WaterAbsorptionController@updateWater');

//Method OF Shaping
Route::resource('method','Tile\MethodOfShapingController');
Route::patch('/method-update','Tile\MethodOfShapingController@updateMethod');

//Nature of Surface
Route::resource('nature','Tile\NatureSurfaceController');
Route::patch('/nature-update','Tile\NatureSurfaceController@updateNature');

//Tiles Per Pack
Route::resource('tile','Tile\TilePerPackController');
Route::patch('/tile-update','Tile\TilePerPackController@updateTiles');

//Tile Information / Master
Route::resource('tile-master','Tile\TileMasterController');
Route::post('/tile-master-update','Tile\TileMasterController@updateTileWithData');
Route::get('/tile-master-data','Tile\TileMasterDataController@masterData');

//GET ALL DATA
Route::get('/brand-data','Tile\GetAllDataController@getBrand');
Route::get('/importer-data','Tile\GetAllDataController@getImporter');
Route::get('/manu-data','Tile\GetAllDataController@getManu');
Route::get('/country-data','Tile\GetAllDataController@getCountry');
Route::get('/nominal-data','Tile\GetAllDataController@getNominal');
Route::get('/thick-data','Tile\GetAllDataController@getThick');
Route::get('/water-range-data','Tile\GetAllDataController@getWaterRange');
Route::get('/method-range-data','Tile\GetAllDataController@getMethod');
Route::get('/nature-data','Tile\GetAllDataController@getNature');
Route::get('/totals-data','Tile\GetAllDataController@getTotalPack');

//PDF Details
Route::get('/getDetailsPdf','Tile\PDFDetailsController@getDetailsPdf');

//Add Batch Number
Route::get('/addBatch','Tile\PDFDetailsController@addBatch');
Route::get('/allBatch','Tile\BatchController@allBatch');

