<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TileMaster extends Model
{
    protected $table = 'tile_master';
    protected $fillable = ['EAN11','MATNR','MAKTX','brand','importers_name','importers_address','manu_name','manu_address','country_origin'
        ,'nominal_size','thickness','water_range','method_shaping','tiles_per_pack','code','kalikasan'];
}
