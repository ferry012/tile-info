<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MethodShaping extends Model
{
    protected $table = 'method_shaping';
    protected $fillable = ['shaping'];
}
