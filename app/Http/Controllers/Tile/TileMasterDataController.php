<?php

namespace App\Http\Controllers\Tile;

use App\Http\Controllers\Controller;
use App\TileMaster;
use App\TileMasterData;
use Illuminate\Http\Request;

class TileMasterDataController extends Controller
{
   public function masterData(Request $request){
       $searchCode = $request->get('code');
       $showAll = $request->get('showAll');
       if ($showAll == 'tile') {
           $query = TileMasterData::select('MATERIAL_MASTER.EAN11', 'MATERIAL_MASTER.MAKTX', 'MATERIAL_MASTER.MATNR')
               ->where('MATERIAL_MASTER.MATKL','=','2309')
               ->where(function($q) use($searchCode){
                   $q->orwhere('MATERIAL_MASTER.EAN11', 'LIKE', "%$searchCode%")
                       ->orWhere('MATERIAL_MASTER.MAKTX', 'LIKE', "%$searchCode%")
                       ->orWhere('MATERIAL_MASTER.MATNR', 'LIKE', "%$searchCode%");
               })
               ->get();


       } else {
           $query = TileMasterData::select('MATERIAL_MASTER.EAN11', 'MATERIAL_MASTER.MAKTX', 'MATERIAL_MASTER.MATNR')
               ->where('MATERIAL_MASTER.MATKL','=','2309')
               ->where(function($q) use($searchCode){
                   $q->orwhere('MATERIAL_MASTER.EAN11', 'LIKE', "%$searchCode%")
                       ->orWhere('MATERIAL_MASTER.MAKTX', 'LIKE', "%$searchCode%")
                       ->orWhere('MATERIAL_MASTER.MATNR', 'LIKE', "%$searchCode%");
               })
               ->get();
       }
       return $query;
   }
}
