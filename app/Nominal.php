<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nominal extends Model
{
    protected $table='nominal';
    protected $fillable = ['size'];
}
