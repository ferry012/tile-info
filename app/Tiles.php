<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tiles extends Model
{
   protected $fillable = ['per_pack'];
}
