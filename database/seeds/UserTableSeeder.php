<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()['cache']->forget('spatie.permission.cache');

        Permission::create(['name' => 'permission_view']);
        Permission::create(['name' => 'permission_create']);
        Permission::create(['name' => 'permission_delete']);
        Permission::create(['name' => 'permission_edit']);
        Permission::create(['name' => 'role_view']);
        Permission::create(['name' => 'role_create']);
        Permission::create(['name' => 'role_delete']);
        Permission::create(['name' => 'role_edit']);
        Permission::create(['name' => 'user_create']);
        Permission::create(['name' => 'user_delete']);
        Permission::create(['name' => 'user_edit']);
        Permission::create(['name' => 'user_view']);

        Permission::create(['name' => 'brand_create']);
        Permission::create(['name' => 'brand_delete']);
        Permission::create(['name' => 'brand_edit']);
        Permission::create(['name' => 'brand_view']);
        Permission::create(['name' => 'country_of_origin_create']);
        Permission::create(['name' => 'country_of_origin_delete']);
        Permission::create(['name' => 'country_of_origin_edit']);
        Permission::create(['name' => 'country_of_origin_view']);
        Permission::create(['name' => 'importers_create']);
        Permission::create(['name' => 'importers_delete']);
        Permission::create(['name' => 'importers_edit']);
        Permission::create(['name' => 'importers_view']);
        Permission::create(['name' => 'manu_create']);
        Permission::create(['name' => 'manu_delete']);
        Permission::create(['name' => 'manu_edit']);
        Permission::create(['name' => 'manu_view']);
        Permission::create(['name' => 'method_create']);
        Permission::create(['name' => 'method_delete']);
        Permission::create(['name' => 'method_edit']);
        Permission::create(['name' => 'method_view']);
        Permission::create(['name' => 'nature_create']);
        Permission::create(['name' => 'nature_delete']);
        Permission::create(['name' => 'nature_edit']);
        Permission::create(['name' => 'nature_view']);
        Permission::create(['name' => 'nominal_create']);
        Permission::create(['name' => 'nominal_delete']);
        Permission::create(['name' => 'nominal_edit']);
        Permission::create(['name' => 'nominal_view']);
        Permission::create(['name' => 'thickness_create']);
        Permission::create(['name' => 'thickness_delete']);
        Permission::create(['name' => 'thickness_edit']);
        Permission::create(['name' => 'thickness_view']);
        Permission::create(['name' => 'tile_create']);
        Permission::create(['name' => 'tile_delete']);
        Permission::create(['name' => 'tile_edit']);
        Permission::create(['name' => 'tile_view']);
        Permission::create(['name' => 'tile_per_pack_create']);
        Permission::create(['name' => 'tile_per_pack_delete']);
        Permission::create(['name' => 'tile_per_pack_edit']);
        Permission::create(['name' => 'tile_per_pack_view']);
        Permission::create(['name' => 'water_create']);
        Permission::create(['name' => 'water_delete']);
        Permission::create(['name' => 'water_edit']);
        Permission::create(['name' => 'water_view']);




        $role = Role::create(['name' => 'super_administrator']);


        $role->givePermissionTo([
            'permission_view',
            'permission_create',
            'permission_delete',
            'permission_edit',
            'role_view',
            'role_create',
            'role_delete',
            'role_edit',
            'user_create',
            'user_delete',
            'user_edit',
            'user_view',
        ]);

        //Create Super admin
        $user = User::create([
            'name' => 'Super Admin',
            'username' => 'vcyadmin',
            'avatar' => 'https://i.pravatar.cc/300',
            'email' => 'itsupport@gmail.com',
            'password' => 'password',
        ]);

        $user->assignRole('super_administrator');
    }
}
