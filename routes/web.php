<?php

use App\Brand;
use App\Model\Invoice;
use App\TileMaster;
use App\TradeMark;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
//
//Auth::routes();
//
//Route::get('/home', 'HomeController@index')->name('home');

//Route::get('{path}', function () {
//    return view('home');
//})->where('path', '(.*)');

//Route::get('/test123', function () {
//    $db = DB::connection('sqlsrv2')->getDatabaseName();
//
//    $query = DB::table('invoices as a')
//        ->join($db . '.dbo.INVENTORY as b', function ($join) {
//            $join->on('b.MATNR', '=', 'a.invoice_MATNR');
//            $join->on('b.WERKS', '=', 'a.invoice_plant');
//        })
//        ->where('a.invoice_date','=','09/02/2021')
//        ->select('b.MATNR','a.invoice_market_MAKTX',
//            DB::raw("'2311_total_qty'=(SELECT SUM(d.qty) FROM invoices AS d
//		            WHERE b.MATNR = d.invoice_MATNR AND d.invoice_plant = 2311)"),
//            DB::raw("'2311_total_LABST'=(SELECT SUM(LABST) FROM VCYSYS820.dbo.INVENTORY AS c
//		            WHERE b.MATNR = c.MATNR AND c.WERKS = 2311)"),
//
//            DB::raw("'2321_total_qty'=(SELECT SUM(d.qty) FROM invoices AS d
//		            WHERE b.MATNR = d.invoice_MATNR AND d.invoice_plant = 2321)"),
//            DB::raw("'2321_total_LABST'=(SELECT SUM(LABST) FROM VCYSYS820.dbo.INVENTORY AS c
//		            WHERE b.MATNR = c.MATNR AND c.WERKS = 2321)"),
//
//            DB::raw("'2331_total_qty'=(SELECT SUM(d.qty) FROM invoices AS d
//		            WHERE b.MATNR = d.invoice_MATNR AND d.invoice_plant = 2331)"),
//            DB::raw("'2331_total_LABST'=(SELECT SUM(LABST) FROM VCYSYS820.dbo.INVENTORY AS c
//		            WHERE b.MATNR = c.MATNR AND c.WERKS = 2331)"),
//
//            DB::raw("'2341_total_qty'=(SELECT SUM(d.qty) FROM invoices AS d
//		            WHERE b.MATNR = d.invoice_MATNR AND d.invoice_plant = 2341)"),
//            DB::raw("'2341_total_LABST'=(SELECT SUM(LABST) FROM VCYSYS820.dbo.INVENTORY AS c
//		            WHERE b.MATNR = c.MATNR AND c.WERKS = 2341)"),
//
//            DB::raw("'2351_total_qty'=(SELECT SUM(d.qty) FROM invoices AS d
//		            WHERE b.MATNR = d.invoice_MATNR AND d.invoice_plant = 2351)"),
//            DB::raw("'2351_total_LABST'=(SELECT SUM(LABST) FROM VCYSYS820.dbo.INVENTORY AS c
//		            WHERE b.MATNR = c.MATNR AND c.WERKS = 2351)"),
//
//            DB::raw("'2361_total_qty'=(SELECT SUM(d.qty) FROM invoices AS d
//		            WHERE b.MATNR = d.invoice_MATNR AND d.invoice_plant = 2361)"),
//            DB::raw("'2361_total_LABST'=(SELECT SUM(LABST) FROM VCYSYS820.dbo.INVENTORY AS c
//		            WHERE b.MATNR = c.MATNR AND c.WERKS = 2361)")
//        )
//        ->groupBy('b.MATNR','a.invoice_market_MAKTX')
//        ->get()
//        ->toArray();
//
//    dd($query);
//});
//Route::get('/test123', function () {
//    $searchEAN11='8851000068314';
//    $searchBrand =null;
//    $searchMAKTX='LUMINARY TILE 15X80 D158892PM-A';
//    $searchImporters_name=null;
//    $searchManu_name = null;
//    $searchCountry_origin = null;
//    $searchNominal_size = null;
//    $searchThickness=null;
//    $searchWater_range=null;
//    $searchMethod_shaping =null;
//    $searchNature_surface = null;
//    $searchTiles_per_pack = null;
//    $searchCode = null;
//    $searchMATNR=null;
//    $q->orwhere('EAN11', 'LIKE', "%$searchEAN11%")
//        ->orwhere('MAKTX', 'LIKE', "%$searchMAKTX%")
//        ->orwhere('brand', 'LIKE', "%$searchBrand%")
//        ->orwhere('importers_name', 'LIKE', "%$searchImporters_name%")
//        ->orwhere('manu_name', 'LIKE', "%$searchManu_name%")
//        ->orwhere('country_origin', 'LIKE', "%$searchCountry_origin%")
//        ->orwhere('nominal_size', 'LIKE', "%$searchNominal_size%")
//        ->orwhere('thickness', 'LIKE', "%$searchThickness%")
//        ->orwhere('water_range', 'LIKE', "%$searchWater_range%")
//        ->orwhere('method_shaping', 'LIKE', "%$searchMethod_shaping%")
//        ->orwhere('kalikasan', 'LIKE', "%$searchNature_surface%")
//        ->orwhere('tiles_per_pack', 'LIKE', "%$searchTiles_per_pack%")
//        ->orwhere('code', 'LIKE', "%$searchCode%")
//        ->orwhere('MATNR', 'LIKE', "%$searchMATNR%");
//    if($searchEAN11 !== null || $searchBrand !== null || $searchMAKTX !== null){
//        if($searchEAN11 !== null){
//            $query = TileMaster::where(function ($q) use ($searchEAN11) {
//                $q->orwhere('EAN11', 'LIKE', "%$searchEAN11%");
//            })  ->orderBy('created_at', 'desc')->paginate(10);
//        }
//     if($searchBrand !== null){
//            $query = TileMaster::where(function ($q) use ($searchBrand) {
//                $q->orwhere('brand', 'LIKE', "%$searchBrand%");
//            })  ->orderBy('created_at', 'desc')->paginate(10);
//        }
//       if($searchMAKTX !== null){
//            $query = TileMaster::where(function ($q) use ($searchMAKTX) {
//                $q->orwhere('MAKTX', 'LIKE', "%$searchMAKTX%");
//            })  ->orderBy('created_at', 'desc')->paginate(10);
//        }
//
//    }
//   else if($searchBrand !== null){
//        $query = TileMaster::where(function ($q) use ($searchBrand) {
//            $q->orwhere('brand', 'LIKE', "%$searchBrand%");
//        })  ->orderBy('created_at', 'desc')->paginate(10);
//    }
//   else if($searchMAKTX !== null){
////       dd('test');
//        $query = TileMaster::where(function ($q) use ($searchMAKTX) {
//            $q->orwhere('MAKTX', 'LIKE', "%$searchMAKTX%");
//        })  ->orderBy('created_at', 'desc')->paginate(10);
//    }
//        else{
//
//            $query = TileMaster::orderBy('created_at', 'desc')->paginate(10);
//        }
//    dd($query);
//
//    else {
//        dd('test');
//        $query = TileMaster::orderBy($orderBy, $orderByDir)->paginate($perPage);
//    }

//});

Route::get('/{any}','AppController@index')->where('any','.*');
