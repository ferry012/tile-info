<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WaterAbsorption extends Model
{
    protected $table = 'water_absorption';
    protected $fillable = ['range'];
}
