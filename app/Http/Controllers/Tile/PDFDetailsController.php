<?php

namespace App\Http\Controllers\Tile;

use App\BatchNumber;
use App\Brand;
use App\Http\Controllers\Controller;
use App\TileMaster;
use Illuminate\Http\Request;

class PDFDetailsController extends Controller
{
   public function getDetailsPdf(Request $request)
   {
       $query = TileMaster::where('tile_master.id','=',$request->id)
           ->join('batch_number as bn','tile_master.id','=','bn.tile_id')
           ->select('bn.number','tile_master.*')
           ->get();

       foreach ($query as $key => $code){

           $imgBrand = Brand::where('brand_name','=',$code->brand)
               ->first();
           $query[$key]['photo'] = $imgBrand->photo;
       }

       return response()->json($query);
   }
   public function addBatch(Request $request)
   {

       $tile = TileMaster::where('id','=',$request->id)->first();

       $batch = new BatchNumber();
       $batch->number = $request->batch;
       $batch->EAN11 = $tile->EAN11;
       $batch->MATNR = $tile->MATNR;
       $batch->MAKTX = $tile->MAKTX;
       $batch->code = $tile->code;
       $batch->tile_id = $tile->id;
       $batch->save();



       return response()->json([
           'success'=>'success',
       ]);
   }
}
