/*auth*/
// import Home from './components/Home'
import Login from './components/authentication/Login'
import Profile from "./components/profile/Profile";

/*users*/
import Users from './components/user/Users'
import UsersTable from "./components/user/UsersTable";
import UserCreate from "./components/user/Create";
import UserView from "./components/user/Show";
import UserEdit from "./components/user/Edit";

/*permission*/
import Permission from "./components/permission/Permission";
import PermissionsTable from "./components/permission/PermissionsTable";
import PermissionCreate from "./components/permission/Create";
import PermissionView from "./components/permission/Show";
import PermissionEdit from "./components/permission/Edit";

/*role*/
import Role from "./components/role/Role";
import RolesTable from "./components/role/RolesTable";
import RoleCreate from "./components/role/Create";
import RoleView from "./components/role/Show";
import RoleEdit from "./components/role/Edit";

/*Tile Master*/
import TileIndex from "./components/tileMaster/index";
import TileParent from "./components/tileMaster/parent";

/*Brand*/
import BrandIndex from "./components/Brand/index";
import BrandParent from "./components/Brand/parent";

/*Importers*/
import ImportersIndex from "./components/importers/index";
import ImportersParent from "./components/importers/parent";

/*Manufacture*/
import ManuIndex from "./components/manufacture/index";
import ManuParent from "./components/manufacture/parent";

/*Country of Origin*/
import CountryIndex from "./components/countryofOrigin/index";
import CountryParent from "./components/countryofOrigin/parent";

/*Nominal Size*/
import NominalIndex from "./components/nominalSize/index";
import NominalParent from "./components/nominalSize/parent";

/*Thickness*/
import ThickIndex from "./components/thickness/index";
import ThickParent from "./components/thickness/parent";

/*Water Absorption*/
import WaterIndex from "./components/waterAbsorption/index";
import WaterParent from "./components/waterAbsorption/parent";

/*Method Of Shaping*/
import ShapingIndex from "./components/shaping/index";
import ShapingParent from "./components/shaping/parent";

/*Nature of Surface & Used*/
import NatureIndex from "./components/natureSurface/index";
import NatureParent from "./components/natureSurface/parent";

/*Tiles Per Pack*/
import TilesIndex from "./components/tilesPerPack/index";
import TilesParent from "./components/tilesPerPack/parent";

/*Batch Number*/
import BatchIndex from "./components/batchNumber/index";
import BatchParent from "./components/batchNumber/parent";

/*Plant*/
import Plant from "./components/plant/Plant";
import PlantsTable from "./components/plant/PlantsTable";

/*passport*/
import Passport from "./components/passport/Passport";

/*errors*/
import Error_404 from "./components/errors/404"


export const routes = [
    // {
    //     path: '/',
    //     name: 'home',
    //     component: Home,
    //     meta: {
    //         title: 'Home',
    //         requiresAuth: true,
    //     }
    // },
    {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            title: 'Login',

        }
    },
    {
        path: '/profile',
        name: 'profile',
        component: Profile,
        meta: {
            title: 'Profile',
            requiresAuth: true,
        }
    },
    {
        path: '/users',
        component: Users,
        children: [
            {
                path: '/',
                component: UsersTable,
                name: 'users-table',
                meta: {
                    title: 'Users',
                    roles: ['administrator','super_administrator','tile_admin'],
                    requiresAuth: true,
                }
            },
            {
                path: 'create',
                component: UserCreate,
                name: 'users-create',
                meta: {
                    title: 'Create User',
                    roles: ['administrator','super_administrator','tile_admin'],
                    requiresAuth: true,
                }
            },
            {
                path: ':id',
                component: UserView,
                name: 'users-view',
                meta: {
                    title: 'View User',
                    roles: ['administrator','super_administrator','tile_admin'],
                    requiresAuth: true,
                }
            },
            {
                path: ':id/edit',
                component: UserEdit,
                name: 'users-edit',
                meta: {
                    title: 'Edit User',
                    roles: ['administrator','super_administrator','tile_admin'],
                    requiresAuth: true,
                }
            }
        ]
    },
    {
        path: '/permissions',
        component: Permission,
        children: [
            {
                path: '/',
                component: PermissionsTable,
                name: 'permission-table',
                meta: {
                    title: 'Permissions',
                    roles: ['administrator','super_administrator'],
                    requiresAuth: true,
                }
            },
            {
                path: 'create',
                component: PermissionCreate,
                name: 'permission-create',
                meta: {
                    title: 'Create Permission',
                    // roles: ['administrator'],
                    roles: ['administrator','super_administrator'],
                    requiresAuth: true,
                }
            },
            {
                path: ':id',
                component: PermissionView,
                name: 'permission-view',
                meta: {
                    title: 'View Permission',
                    roles: ['administrator','super_administrator'],
                    requiresAuth: true,
                }
            },
            {
                path: ':id/edit',
                component: PermissionEdit,
                name: 'permission-edit',
                meta: {
                    title: 'Edit Permission',
                    roles: ['administrator','super_administrator'],
                    requiresAuth: true,
                }
            }
        ]
    },
    {
        path: '/roles',
        component: Role,
        children: [
            {
                path: '/',
                component: RolesTable,
                name: 'role-table',
                meta:

                {
                    title: 'Roles',
                    roles: ['administrator','super_administrator','tile_admin'],
                    requiresAuth: true,
                }

            },
            {
                path: 'create',
                component: RoleCreate,
                name: 'role-create',
                meta:

                {
                    title: 'Create Role',
                    roles: ['administrator','super_administrator','tile_admin'],
                    requiresAuth: true,
                }

            },
            {
                path: ':id',
                component: RoleView,
                name: 'role-view',
                meta: {
                    title: 'View Role',
                    roles: ['administrator','super_administrator','tile_admin'],
                    requiresAuth: true,
                }
            },
            {
                path: ':id/edit',
                component: RoleEdit,
                name: 'role-edit',
                meta: {
                    title: 'Edit Role',
                    roles: ['administrator','super_administrator','tile_admin'],
                    requiresAuth: true,
                }
            }
        ]
    },
    /*Batch Number*/
    {
        path: '/batch',
        component: BatchParent,
        children: [
            {
                path: '/',
                component: BatchIndex ,
                name: 'batch-table',
                meta:

                    {
                        title: 'Roles',
                        roles: ['administrator','super_administrator','tile_admin'],
                        requiresAuth: true,
                    }

            },
        ]
    },

    /*Brand*/
    {
        path: '/brand',
        component: BrandParent ,
        children: [
            {
                path: '/',
                component: BrandIndex ,
                name: 'brand-table',
                meta:

                    {
                        title: 'Roles',
                        roles: ['administrator','super_administrator','tile_admin'],
                        requiresAuth: true,
                    }

            },
        ]
    },
    /*importers*/
    {
        path: '/importers',
        component: ImportersParent,
        children: [
            {
                path: '/',
                component: ImportersIndex,
                name: 'importers-table',
                meta:

                    {
                        title: 'Roles',
                        roles: ['administrator','super_administrator','tile_admin'],
                        requiresAuth: true,
                    }

            },
        ]
    },
    /*Manufacture*/
    {
        path: '/manu',
        component: ManuParent,
        children: [
            {
                path: '/',
                component: ManuIndex,
                name: 'manu-table',
                meta:

                    {
                        title: 'Roles',
                        roles: ['administrator','super_administrator','tile_admin'],
                        requiresAuth: true,
                    }

            },
        ]
    },
    /*Country of Origin*/
    {
        path: '/country-of-origin',
        component: CountryParent,
        children: [
            {
                path: '/',
                component: CountryIndex,
                name: 'country-table',
                meta:

                    {
                        title: 'Roles',
                        roles: ['administrator','super_administrator','tile_admin'],
                        requiresAuth: true,
                    }

            },
        ]
    },
    /*Nominal Size*/
    {
        path: '/nominal-size',
        component: NominalParent,
        children: [
            {
                path: '/',
                component: NominalIndex,
                name: 'nominal-table',
                meta:

                    {
                        title: 'Roles',
                        roles: ['administrator','super_administrator','tile_admin'],
                        requiresAuth: true,
                    }

            },
        ]
    },
    /*Thickness*/
    {
        path: '/thickness',
        component: ThickParent,
        children: [
            {
                path: '/',
                component: ThickIndex,
                name: 'thickness-table',
                meta:

                    {
                        title: 'Roles',
                        roles: ['administrator','super_administrator','tile_admin'],
                        requiresAuth: true,
                    }

            },
        ]
    },
    /*Water Absorption*/
    {
        path: '/water',
        component: WaterParent,
        children: [
            {
                path: '/',
                component: WaterIndex,
                name: 'water-table',
                meta:

                    {
                        title: 'Roles',
                        roles: ['administrator','super_administrator','tile_admin'],
                        requiresAuth: true,
                    }

            },
        ]
    },
    /*Shaping*/
    {
        path: '/shaping',
        component: ShapingParent,
        children: [
            {
                path: '/',
                component: ShapingIndex,
                name: 'shaping-table',
                meta:

                    {
                        title: 'Roles',
                        roles: ['administrator','super_administrator','tile_admin'],
                        requiresAuth: true,
                    }

            },
        ]
    },
    /*Nature of Surface*/
    {
        path: '/nature',
        component: NatureParent,
        children: [
            {
                path: '/',
                component: NatureIndex,
                name: 'nature-table',
                meta:

                    {
                        title: 'Roles',
                        roles: ['administrator','super_administrator','tile_admin'],
                        requiresAuth: true,
                    }

            },
        ]
    },
    /*Tiles Per Pack*/
    {
        path: '/tiles-per-pack',
        component: TilesParent,
        children: [
            {
                path: '/',
                component: TilesIndex,
                name: 'tiles-per-pack-table',
                meta:

                    {
                        title: 'Roles',
                        roles: ['administrator','super_administrator','tile_admin'],
                        requiresAuth: true,
                    }

            },
        ]
    },
    /*Tiles*/
    {
        path: '/tiles',
        component: TileParent,
        children: [
            {
                path: '/',
                component: TileIndex,
                name: 'tiles-table',
                meta:

                    {
                        title: 'Roles',
                        roles: ['administrator','super_administrator','tile_admin'],
                        requiresAuth: true,
                    }

            },
        ]
    },
    {
        path: '/plants',
        component: Plant,
        children: [
            {
                path: '/',
                component: PlantsTable,
                name: 'plant-table',
                meta: {
                    title: 'Plant',
                    roles: ['administrator'],
                    requiresAuth: true,
                }
            }
            ]
    },
    {
        path: '/passport',
        component: Passport,
        meta: {
            title: 'Passport',
            requiresAuth: true,
            roles: ['administrator'],
        }
    },
    // {
    //     path: '/404',
    //     name: 'error_404',
    //     component: Error_404,
    //     meta: {
    //         title: 'Error 404',
    //     }
    // },
    // {path: '*', redirect: '/404'},

];
