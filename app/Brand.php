<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table ='trademark';
    protected $fillable = ['brand_name','photo'];
}
